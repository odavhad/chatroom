#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define MAX_CLIENTS 100
#define BUFFER_SIZE 2048
#define NAME_LENGTH 32

static _Atomic unsigned int cli_count = 0;
static int uid = 10;

// Client structure
typedef struct {
    struct sockaddr_in address;
    int sockfd;
    int uid;
    char name[NAME_LENGTH];
} client_t;

client_t* clients[MAX_CLIENTS];

pthread_mutex_t clients_mutex = PTHREAD_MUTEX_INITIALIZER;

void str_overwrite_stdout() {
    printf("\r%s", ">");
    fflush(stdout);
}

void str_trim_lf(char* arr, int length) {
    for (int i = 0; i < length; ++i) {
        if (arr[i] == '\n') {
            arr[i] = '\0';
            break;
        }
    }
}

void queue_add(client_t* cl) {
    pthread_mutex_lock(& clients_mutex);

    for (int i = 0; i < MAX_CLIENTS; ++i) {
        if (!clients[i]) {
            clients[i] = cl;
            break;
        }
    }

    pthread_mutex_unlock(& clients_mutex);
}

void queue_remove(int uid) {
    pthread_mutex_lock(& clients_mutex);

    for (int i = 0; i < MAX_CLIENTS; ++i) {
        if (clients[i]) {
            if (clients[i]->uid == uid) {
                clients[i] = NULL;
                break;
            }
        }
    }

    pthread_mutex_unlock(& clients_mutex);    
}

void send_message(char* str, int uid) {
    pthread_mutex_lock(& clients_mutex);

    for (int i = 0; i < MAX_CLIENTS; ++i) {
        if (clients[i]) {
            if (clients[i]->uid != uid) {
                if (write(clients[i]->sockfd, str, strlen(str)) < 0) {
                    printf("ERROR: write to descriptor failed\n");
                    break;
                }
            }
        }
    }

    pthread_mutex_unlock(& clients_mutex);
}

void print_ip_addr(struct sockaddr_in addr) {
    printf("%d.%d.%d.%d",
        addr.sin_addr.s_addr & 0xff,
        (addr.sin_addr.s_addr & 0xff00) >> 8,
        (addr.sin_addr.s_addr & 0xff0000) >> 16,
        (addr.sin_addr.s_addr & 0xff000000) >> 24);
}

void* handle_client(void *arg) {
    char buff[BUFFER_SIZE];
    char name[NAME_LENGTH];
    int leave_flag = 0;
    cli_count++;

    client_t* cli = (client_t*) arg;

    // Name 
    if (recv(cli->sockfd, name, NAME_LENGTH, 0) <= 0 || strlen(name) < 2 || strlen(name) >= NAME_LENGTH - 1) {
        printf("ERROR: Incorrect name\n");
        printf("Enter your name correctly\n");
        leave_flag = 1;
    } else {
        strcpy(cli->name, name);
        sprintf(buff, "%s has joined\n", cli->name);
        printf("%s", buff);
        send_message(buff, cli->uid);
    }

    bzero(buff, BUFFER_SIZE);

    while (1) {
        if (leave_flag) 
            break;
        
        int receive = recv(cli->sockfd, buff, BUFFER_SIZE, 0);
        if (receive > 0) {
            if (strlen(buff) > 0) {
                send_message(buff, cli->uid);
                str_trim_lf(buff, strlen(buff));
                printf("%s -> %s", buff, cli->name);
            } else if (receive == 0 || strcmp(buff, "exit") == 0) {
                sprintf(buff, "%s has left\n", cli->name);
                printf("%s", buff);
                send_message(buff, cli->uid);
                leave_flag = 1;
            } else {
                printf("ERROR: -1\n");
                leave_flag = 1;
            }

            bzero(buff, BUFFER_SIZE);
        }

        bzero(buff, BUFFER_SIZE);
    }

    close(cli->sockfd);
    queue_remove(cli->uid);
    free(cli);
    cli_count--;
    pthread_detach(pthread_self());

    return NULL;
}

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("Usage: %s <port>\n", argv[0]);
        return EXIT_FAILURE;
    }

    char* ip = "127.0.0.1";
    int port = atoi(argv[1]);

    int option = 1;
    int listenfd = 0, connfd = 0;
    struct sockaddr_in serv_addr;
    struct sockaddr_in cli_addr;
    pthread_t tid;

    // Socket settings
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip);
    serv_addr.sin_port = htons(port);

    // Signals
    signal(SIGPIPE, SIG_IGN);

    if (setsockopt(listenfd, SOL_SOCKET, (SO_REUSEPORT | SO_REUSEADDR), (char*)& option, sizeof(option)) < 0) {
        printf("ERROR: setsockopt\n");
        return EXIT_FAILURE;
    }

    // Bind
    if (bind(listenfd, (struct sockaddr*)& serv_addr, sizeof(serv_addr)) < 0) {
        printf("ERROR: bind\n");
        return EXIT_FAILURE;
    }

    // Listen
    if (listen(listenfd, 10) < 0) {
        printf("ERROR: listen\n");
        return EXIT_FAILURE;
    }

    printf("<----------- Welcome to the Chatroom ----------->\n");
    while (1) {
        socklen_t cli_len = sizeof(cli_addr);
        connfd = accept(listenfd, (struct sockaddr*)& cli_addr, & cli_len);

        // check for maximum clients
        if (cli_count + 1 == MAX_CLIENTS) {
            printf("Maximum number of clients connected. \nConnection Rejected.\n");
            print_ip_addr(cli_addr);
            close(connfd);
            continue;
        }

        // Client settings
        client_t* cli = (client_t*)malloc(sizeof(client_t));
        cli->address = cli_addr;
        cli->sockfd = connfd;
        cli->uid = uid++;

        // Add client to queue
        queue_add(cli);
        pthread_create(& tid, NULL, & handle_client, (void*) cli);

        // Reduce CPU usage
        sleep(1);
    }

    return EXIT_SUCCESS;
}
